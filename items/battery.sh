#!/bin/bash

battery=(
  script="$PLUGIN_DIR/battery.sh"
  icon.font="$FONT:Regular:16.0"
  icon.padding_right=0
  icon.padding_left=0
  label.width=35
  label.align=right
  padding_left=0
  padding_right=0
  update_freq=60
  update_freq=120
  updates=on
)

sketchybar --add item battery right      \
           --set battery "${battery[@]}" \
           --subscribe battery power_source_change system_woke
